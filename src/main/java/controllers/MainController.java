package controllers;

import entities.Employee;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import repositories.AddressRepository;
import repositories.EmployeeRepository;
import repositories.WorkplaceRepository;

import java.net.URI;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/entities")
public class MainController {


    @Autowired
    private  EmployeeRepository employeeRepository;
    @Autowired
    private  WorkplaceRepository workplaceRepository;
    @Autowired
    private  AddressRepository addressRepository;


    @GetMapping("/employees")
    public List<Employee> retrieveAllEmployees() {
        return employeeRepository.findAll();
    }

    @GetMapping("/employees/{id}")
    public Employee retrieveEmployee(@PathVariable long id )throws EmployeeNotFoundException {
        Optional<Employee> employee = employeeRepository.findById(id);

        if (!employee.isPresent())
            throw new EmployeeNotFoundException("id-" + id);

        return employee.get();
    }

    @PostMapping("/employees")
    public Employee createEmployee(@RequestBody Employee employee)

    {
        employee.getAddresses().forEach(address -> {
            addressRepository.save(address);
        });
        workplaceRepository.save(employee.getWorkplace());
        employeeRepository.save(employee);

        return employee;

    }
    @PutMapping("/employees/{id}")
    public ResponseEntity<Object> updateEmployee(@RequestBody Employee employee, @PathVariable long id) {

        Optional<Employee>  employeeOptional = employeeRepository.findById(id);

        if (!employeeOptional.isPresent())
            return ResponseEntity.notFound().build();

        employeeRepository.save(employee);

        return ResponseEntity.noContent().build();
    }


    @DeleteMapping("/employee/{id}")
    public void deleteEmployee(@PathVariable long id) {
        employeeRepository.deleteById(id);
    }


}
