package entities;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "workplace")
public class Workplace {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String companyName;

    @OneToMany
    private Set<Employee> employees = new HashSet<>();

    public Long getId() {
        return id;
    }

    public String getCompanyName() {
        return companyName;
    }

    public Set<Employee> getEmployees() {
        return employees;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public void setEmployees(Set<Employee> employees) {
        this.employees = employees;
    }
}

