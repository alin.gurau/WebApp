package entities;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
public class Employee {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String firstName;
    private String secondName;

    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(name = "employee_address", joinColumns = {@JoinColumn(name="employee_id")}, inverseJoinColumns = {@JoinColumn(name = "address_id")})
    Set<Address> addresses = new HashSet<>();


    @ManyToOne
    private Workplace workplace;


    public Long getId() {
        return id;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getSecondName() {
        return secondName;
    }

    public Set<Address> getAddresses() {
        return addresses;
    }

    public Workplace getWorkplace() {
        return workplace;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setSecondName(String secondName) {
        this.secondName = secondName;
    }

    public void setAddresses(Set<Address> addresses) {
        this.addresses = addresses;
    }

    public void setWorkplace(Workplace workplace) {
        this.workplace = workplace;
    }
}
